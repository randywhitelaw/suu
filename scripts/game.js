﻿var Game = (function () {

    var Game = function (max) {

        var self = this;

        self.max = max || 10;
        self.onStart = function () { };
        self.onTooLow = function () { };
        self.onTooHigh = function () { };
        self.onCorrect = function () { };

        self.randomNumber = 0;
        self.guessCount = 0;
        self.currentGuess = 0;
    };

    Game.prototype.start = function () {
        var self = this;

        self.randomNumber = self.getRandomNumber(self.max);
        self.guessCount = 0;
        self.onStart.apply(self, [self]);
    }

    Game.prototype.getRandomNumber = function (max) {
        var self = this;

        return Math.ceil(Math.random() * max);
    };

    Game.prototype.guess = function (num) {
        var self = this;

        var number = self.parseNumber(num);

        self.guessCount++;
        self.currentGuess = number;

        if (self.currentGuess < self.randomNumber) {
            self.onTooLow.apply(self, [self]);
        }
        else if (self.currentGuess > self.randomNumber) {
            self.onTooHigh.apply(self, [self]);
        }
        else {
            self.onCorrect.apply(self, [self]);
        }
    };

    Game.prototype.parseNumber = function (num) {
        var self = this;

        var number = parseInt(num);

        if (isNaN(number)) {
            throw new Error("\"" + num + "\" is an invalid number.");
        }

        if (number < 1 || number > self.max) {
            throw new Error("Guess must be between 1 and " + self.max + ".");
        }

        return number;
    };

    return Game;
})();