﻿$(function () {

    var max = 500;

    var elements = {
        pageTitle: document.title,
        results: $("#results"),
        gameTitle: $("#gameTitle"),
        numberField: $("#numberField"),
        startButton: $("#startButton"),
        gameBoard: $("#gameBoard"),
        guessBox: $("#guessBox"),
        guessButton: $("#guessButton")
    };

    var templates = {
        pageTitle: Handlebars.compile($("#pageTitleTemplate").html()),
        gameTitle: Handlebars.compile($("#gameTitleTemplate").html()),
        tooLow: Handlebars.compile($("#lowTemplate").html()),
        tooHigh: Handlebars.compile($("#highTemplate").html()),
        correct: Handlebars.compile($("#correctTemplate").html()),
        invalid: Handlebars.compile($("#invalidTemplate").html())
    };

    var game = new Game(max);

    game.onStart = function (game) {
        elements.pageTitle = templates.pageTitle(game);
        elements.gameTitle.html(templates.gameTitle(game));
        elements.results.empty();
        elements.numberField.val("");
        elements.startButton.hide();
        elements.guessBox.show();
        elements.gameBoard.show(function () {
            elements.numberField.select();
        });
    };


    game.onTooLow = function (game) {
        elements.results.prepend(templates.tooLow(game));
    };

    game.onTooHigh = function (game) {
        elements.results.prepend(templates.tooHigh(game));
    };

    game.onCorrect = function (game) {
        var context = {
            currentGuess: game.currentGuess,
            guessCount: game.guessCount,
            plural: (game.guessCount == 1 ? "" : "es")
        };

        elements.results.prepend(templates.correct(context));
        elements.guessBox.hide();
        elements.startButton.html("New Game").show();
    }

    game.start();

    elements.numberField.on("keyup", function (e) {
        if (e.keyCode == 13) {
            guess();
        }
    });

    elements.guessButton.on("click", function (e) {
        e.preventDefault();
        guess();
    });

    elements.startButton.on("click", function (e) {
        e.preventDefault();
        game.start();
    });

    var guess = function () {
        var num = elements.numberField.val();

        try {
            game.guess(num);
        }
        catch (e) {
            elements.results.prepend(templates.invalid(e));
        }

        elements.numberField.select();
    };

});